﻿using System;
using System.Threading;

namespace Scripts.Model
{
  public class Warrior
  {
    private const int ChargeTimeInMilliseconds = 500;
  
    private int health;
    private int damage;
    private int bullets;
  
    public float Speed { get; private set; }
  
    private bool isWeaponCharged;
  
    public event Action OnDead;
    public event Action OnHit;
    public event Action OnBulletsEnd;
    public event Action<int> OnShot;
  
    public Warrior( int health, int damage, int bullets, float speed )
    {
      this.health  = health;
      this.damage  = damage;
      this.bullets = bullets;
      this.Speed   = speed;
  
      isWeaponCharged = true;
    }
  
    public void Shoot()
    {
      if ( bullets > 0 )
      {
        if ( isWeaponCharged )
        {
          --bullets;
          isWeaponCharged = false;
          OnShot( bullets );
  
          Thread t = new Thread( delegate()
          {
            Thread.Sleep( ChargeTimeInMilliseconds );
            isWeaponCharged = true;
          } );
  
          t.Start();
        }
      } else
      {
        OnBulletsEnd();
      }
    }
  
    public void Hit( int receiveDamage )
    {
      health -= receiveDamage;
      
      if ( health <= 0 )
      {
        OnDead();
      } else
      {
        OnHit();
      }
    }
  }
}
