﻿using System;
using System.Collections.Generic;

namespace Scripts.Model
{
  public class Battle
  {
    public Warrior Player { get; private set; }
    public Warrior Enemy { get; private set; }
  
    private Queue<Warrior> enemiesStock = new Queue<Warrior>();
  
    public event Action<TeamWon> OnFinish;
    public event Action OnAddNewAnemy;
  
    public Battle( byte enemyCount )
    {
      Player = new Warrior( 20, 100, 50, 4f );
      Player.OnDead += OnPlayerDead;
      Player.OnBulletsEnd += OnPlayerBulletEnd;
  
      for ( int i = 0; i < enemyCount; i++ )
      {
        Warrior enemy  = new Warrior( 100,  20, 50,  1f );
        enemiesStock.Enqueue( enemy );
      }
  
      AddEnemy();
    }
  
    private void AddEnemy()
    {
      Enemy = enemiesStock.Dequeue();
      Enemy.OnDead += OnEnemyDead;
      Player.OnBulletsEnd += OnEnemyBulletEnd;
    }
  
    private void AddEnemyOrFinish()
    {
      if ( enemiesStock.Count > 0 )
      {
        AddEnemy();
        OnAddNewAnemy();
      } else
      {
        OnFinish( TeamWon.First );
      }
    }
  
    private void OnEnemyDead()
    {
      Enemy.OnDead -= OnEnemyDead;
      Enemy.OnBulletsEnd -= OnEnemyBulletEnd;
      AddEnemyOrFinish();
    }
  
    private void OnPlayerDead()
    {
      Player.OnDead -= OnPlayerDead;
      Player.OnBulletsEnd -= OnPlayerBulletEnd;
      OnFinish( TeamWon.Second );
    }
  
    private void OnPlayerBulletEnd()
    {
      OnFinish( TeamWon.Draw );
    }
  
    private void OnEnemyBulletEnd()
    {
      AddEnemyOrFinish();
    }
  
    public void timeOver()
    {
      OnFinish( TeamWon.Draw );
    }
  
    public enum TeamWon
    {
      First,
      Second,
      Draw
    }
  }
}

