﻿using System;
using System.Collections;
using Scripts.Common;
using Scripts.Model;
using UnityEngine;

namespace Scripts.Controler
{
  public class BattleControler : Singleton<BattleControler>
  {
    private BattleControler() { }
  
    [SerializeField] private float finishBattleTime;
  
    [SerializeField] private float enemyDistanceFromScreenBorder;
    [SerializeField] private float playerDistanceFromScreenBorder;
    [SerializeField] private float moveRangeDistanceFromScreenBorder;
  
    [SerializeField] private PlayerControler playerControler;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private Transform parentEnemyObject;
  
    private GameObject currentEnemyObject;
  
    public event Action OnOpenBattle;
    public event Action<Battle.TeamWon> OnCloseBattle;
    public event Action<float> OnTimeToEndBattle;
  
    private Battle battle;
  
    private Coroutine waitFotBattleTimeEndCoroutine;
  
    public void CreateSingleBattle( byte enemyCount, float battleTime )
    {
      battle = new Battle( enemyCount );
  
      battle.OnAddNewAnemy  += OnAddNewAnemy;
      battle.OnFinish       += OnFinishBattle;
  
      playerControler.Launch( battle.Player, StartPlayerPosition(), MoveRange(), Vector2.right );
      CreateNewEnemy();
  
      float endBattleTime = Time.time + battleTime;
      waitFotBattleTimeEndCoroutine = StartCoroutine( WaitFotBattleTimeEnd( endBattleTime ) );
  
      OnOpenBattle();
    }
  
    private void CreateNewEnemy()
    {
      currentEnemyObject = Instantiate( enemyPrefab, parentEnemyObject );
      currentEnemyObject.GetComponent<AIControler>().Launch( battle.Enemy, StartEnemyPosition(), MoveRange(), Vector2.left );
    }
  
    private Vector2 StartPlayerPosition()
    {
       return new Vector2( ( - Camera.main.ScreenToWorldPoint( new Vector3( Screen.width, Screen.height, 0 ) ).x + playerDistanceFromScreenBorder ), 0 );
    }
  
    private float MoveRange()
    {
      return Camera.main.ScreenToWorldPoint( new Vector3( Screen.width, Screen.height, 0 ) ).y - moveRangeDistanceFromScreenBorder;
    }
  
    private Vector2 StartEnemyPosition()
    {
      return new Vector2( ( Camera.main.ScreenToWorldPoint( new Vector3( Screen.width, Screen.height, 0 ) ).x - enemyDistanceFromScreenBorder ), 0 );
    }
  
    private IEnumerator WaitFotBattleTimeEnd( float endBattleTime )
    {
      WaitForSeconds waitOneSecond = new WaitForSeconds( 1.0f );
  
      while( true )
      {
        float timeToEndBattle = endBattleTime - Time.time;
        OnTimeToEndBattle( timeToEndBattle );
  
        if( timeToEndBattle <= 0 )
        {
          battle.timeOver();
          yield break;
        }
  
        yield return waitOneSecond;
      }
    }
  
    private void OnAddNewAnemy()
    {
      CreateNewEnemy();
    }
  
    private void OnFinishBattle( Battle.TeamWon team )
    {
      StopCoroutine( waitFotBattleTimeEndCoroutine );
      StartCoroutine( ClosingBattle( team ) );
    }
  
    private IEnumerator ClosingBattle( Battle.TeamWon team )
    {
      yield return new WaitForSeconds( finishBattleTime );
  
      OnCloseBattle( team );
  
      if ( team == Battle.TeamWon.First )
      {
        playerControler.clear();
      } else
      if ( team == Battle.TeamWon.Second )
      {
        Destroy( currentEnemyObject );
      }
    }
  }
}
