﻿using Scripts.Common;
using Scripts.Model;
using Scripts.View.Components;
using UnityEngine;

namespace Scripts.Controler
{
  public class PlayerControler : MonoBehaviour
  {
    [SerializeField] private GameObject playerPrefab;
  
    private Warrior playerWarrior;
    private WarriorComponent playerComponent;
    private Vector2 direction;
    private BoxCollider2D sightCollider;
    private bool canControl;
  
    public void Launch( Warrior player, Vector2 startPosition, float moveRange, Vector2 shootDirection )
    {
      canControl = true;
      direction = shootDirection;
      playerWarrior = player;
  
      playerComponent = Instantiate( playerPrefab, transform ).GetComponent<WarriorComponent>();
      sightCollider = playerComponent.GetComponent<BoxCollider2D>();
  
      playerComponent.Adjust( startPosition, moveRange, shootDirection, player.Speed );
  
      playerComponent.OnHit += ( damage ) => player.Hit( damage );
      player.OnDead += OnPlayerDead;
      player.OnShot += OnPlayerShot;
      player.OnHit += OnPlayerHit;
    }
  
    public void clear()
    {
      if ( playerComponent == null || playerComponent.gameObject == null )
        return;

      Destroy( playerComponent.gameObject );
    }
  
    public void Shoot()
    {
      if ( !canControl )
        return;
  
      playerWarrior.Shoot();
    }
  
    public void MoveUp( bool isMove )
    {
      if ( !canControl )
        return;
  
      playerComponent.Walk( WarriorComponent.MoveDirection.Left, isMove );
    }
  
    public void MoveDown( bool isMove )
    {
      if ( !canControl )
        return;
  
      playerComponent.Walk( WarriorComponent.MoveDirection.Right, isMove );
    }
  
    private void OnPlayerShot( int damage )
    {
      playerComponent.Shoot( direction, damage, TagsName.Enemy );
    }
  
    private void OnPlayerDead()
    {
      playerComponent.Death();
      sightCollider.enabled = false;
      canControl = false;
    }
  
    private void OnPlayerHit()
    {
      playerComponent.Hit();
    }
  }
}

