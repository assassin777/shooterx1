﻿using Scripts.Common;
using Scripts.Model;
using Scripts.View.Components;
using UnityEngine;

namespace Scripts.Controler
{
  public class AIControler : MonoBehaviour
  {
    private WarriorComponent enemyComponent;
  
    [SerializeField] private float minTimeChangeDirection;
    [SerializeField] private float maxTimeChangeDirection;
  
    [SerializeField] private float minTimeNextFindPlayer;
    [SerializeField] private float maxTimeNextFindPlayer;
  
    [SerializeField] private float minTimePrepareToShoot;
    [SerializeField] private float maxTimePrepareToShoot;
  
    private float timeChangeDirection;
    private float timeNextFindPlayer;
    private float timePrepareToShoot;
  
    private float changeDirectionTimer;
    private float nextFindPlayerTimer;
    private float prepareToShootTimer;
  
    private bool preparingToShoot;
    private bool sawPlayer;
    private bool canFindPlayer;
    private bool canChangeDirection;
    private bool isWalk;
    private bool isAlive;
  
    private Vector2 directon;
  
    private Warrior enemyWarrior;
  
    private void Awake()
    {
      enemyComponent = GetComponent<WarriorComponent>();
    }
  
    private void Start()
    {
      canChangeDirection = true;
      canFindPlayer = true;
      isWalk = true;
      isAlive = true;
    }
  
    public void Launch( Warrior enemy, Vector2 startPosition, float moveRange, Vector2 shootDirection )
    {
      directon = shootDirection;
      enemyWarrior = enemy;
  
      enemyComponent.Adjust( startPosition, moveRange, shootDirection, enemy.Speed );
      
      enemyComponent.OnHit += ( damage ) => enemy.Hit( damage );
      enemy.OnDead += OnEnemyDead;
      enemy.OnShot += OnEnemyShot;
      enemy.OnHit += OnEnemyHit;
    }
  
    private void Update()
    {
      if ( !isAlive )
        return;
  
      if ( preparingToShoot )
      {
        if ( prepareToShootTimer >= timePrepareToShoot )
        {
          isWalk = true;
          enemyWarrior.Shoot();
          preparingToShoot = false;
        } else
        {
          prepareToShootTimer += Time.deltaTime;
        }
      } else
      if ( sawPlayer )
      {
        isWalk = false;
        sawPlayer = false;
        prepareToShootTimer = 0;
        timePrepareToShoot = UnityEngine.Random.Range( minTimePrepareToShoot, maxTimePrepareToShoot );
  
        preparingToShoot = true;
      } else
      if ( canFindPlayer )
      {
        canFindPlayer = false;
        nextFindPlayerTimer = 0;
        timeNextFindPlayer = UnityEngine.Random.Range( minTimeNextFindPlayer, maxTimeNextFindPlayer );
  
        RaycastHit2D hit = Physics2D.Raycast( transform.position + new Vector3( -2, 0, 0 ), directon );
        if ( hit.collider != null )
        {
          if ( hit.transform.gameObject.CompareTag( "Player" ) )
          {
            sawPlayer = true;
          }
        }
      }
  
      if ( !canFindPlayer )
      {
        if ( nextFindPlayerTimer >= timeNextFindPlayer )
        {
          canFindPlayer = true;
        } else
        {
          nextFindPlayerTimer += Time.deltaTime;
        }
      }
  
      if ( canChangeDirection )
      {
        canChangeDirection = false;
        changeDirectionTimer = 0;
        timeChangeDirection = UnityEngine.Random.Range( minTimeChangeDirection, maxTimeChangeDirection );
  
        enemyComponent.Walk( moveDirection(), isWalk );
      } else
      {
        if ( changeDirectionTimer >= timeChangeDirection )
        {
          canChangeDirection = true;
        } else
        {
          changeDirectionTimer += Time.deltaTime;
        }
      }
    }
  
    private WarriorComponent.MoveDirection moveDirection()
    {
      if ( UnityEngine.Random.Range( 0, 100 ) > 50 )
      {
        return WarriorComponent.MoveDirection.Left;
      } else
      {
        return WarriorComponent.MoveDirection.Right;
      }
    }
  
    private void OnEnemyShot( int damage )
    {
      enemyComponent.Shoot( directon, damage, TagsName.Player );
    }
  
    private void OnEnemyDead()
    {
      isAlive = false;
      enemyComponent.Death();
    }
  
    private void OnEnemyHit()
    {
      enemyComponent.Hit();
    }
  }
}

