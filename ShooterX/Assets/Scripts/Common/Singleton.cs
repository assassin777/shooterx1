﻿using UnityEngine;

namespace Scripts.Common
{
  public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
  {
    private static bool isDestroyed = false;
    
    private static T instance;
    
    private static object lockObj = new object();
    
    public static T Instance
    {
      get
      {
        if ( isDestroyed )
        {
          Debug.LogWarning( string.Format( "[Singleton] Instance '{0}' already destroyed on application quit. Won't create again - returning null.", typeof(T) ) );
          return null;
        }
        
        lock( lockObj )
        {
          if ( instance == null )
          {
            instance = (T) FindObjectOfType( typeof(T) );
            
            if ( FindObjectsOfType( typeof(T) ).Length > 1 )
            {
              Debug.LogError( "[Singleton] Something went really wrong - there should never be more than 1 singleton! Reopening the scene might fix it." );
              return instance;
            }
          }
          
          return instance;
        }
      }
    }
    
    private void OnDestroy()
    {
      isDestroyed = true;
    }
  }
}
