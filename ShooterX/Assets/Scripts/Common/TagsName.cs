﻿
namespace Scripts.Common
{
  public static class TagsName
  {
    public const string Player = "Player";
    public const string Enemy = "Enemy";
    public const string Wall = "Wall";
  }
}
