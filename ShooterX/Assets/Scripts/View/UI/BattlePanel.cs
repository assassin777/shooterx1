﻿using System;
using Scripts.Controler;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.View.UI
{
  public class BattlePanel : MonoBehaviour
  {
    [SerializeField] private Text timeToEndBattleText;
    [SerializeField] private PlayerControler playerControler;
  
    private void Awake()
    {
      BattleControler.Instance.OnTimeToEndBattle += OnTimeToEndBattle;
    }
  
    private void OnTimeToEndBattle( float timeToEndBattle )
    {
      TimeSpan ts = TimeSpan.FromSeconds( timeToEndBattle );
      timeToEndBattleText.text = string.Format( "Time to end {0}:{1}", ts.Minutes, ts.Seconds );
    }
  
    public void LeftButtonDown()
    {
      playerControler.MoveUp( true );
    }
  
    public void LeftButtonUp()
    {
      playerControler.MoveUp( false );
    }
  
    public void RightButtonDown()
    {
      playerControler.MoveDown( true );
    }
  
    public void RightButtonUp()
    {
      playerControler.MoveDown( false );
    } 
  
    public void ShootButtonClick()
    {
      playerControler.Shoot();
    }
  }
}
