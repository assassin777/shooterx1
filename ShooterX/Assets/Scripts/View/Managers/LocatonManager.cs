﻿using System;
using UnityEngine;

namespace Scripts.View.Managers
{
  public class LocatonManager :MonoBehaviour
  {
    private SpriteRenderer[] spArray;
    [SerializeField] private Sprite[] spriteArray;
  
    private int spriteArrayLength;
  
    private void Awake()
    {
      spArray = GetComponentsInChildren<SpriteRenderer>();
      spriteArrayLength = spriteArray.Length;
    }
  
    private void OnEnable()
    {
      Sprite randowSprite = spriteArray[UnityEngine.Random.Range( 0, spriteArrayLength )];
      Array.ForEach( spArray, sp => sp.sprite = randowSprite );
    }
  }
}

