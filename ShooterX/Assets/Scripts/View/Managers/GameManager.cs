﻿using Scripts.Common;
using Scripts.Controler;
using UnityEngine;

namespace Scripts.View.Managers
{
  public class GameManager : Singleton<GameManager>
  {
    [SerializeField] private byte enemyCount = 5;
    [SerializeField] private float battleTime = 60 * 10 ; // in seconds
  
    private void Start()
    {
      OpenMainMenu();
  	}
  
    public void OpenMainMenu()
    {
      UIManager.Instance.OpenMainMenu();
    }
  
    public void OpenSingleBattle()
    {
      BattleControler.Instance.OnOpenBattle += OnOpenBattle;
      BattleControler.Instance.CreateSingleBattle( enemyCount, battleTime );
    }
  
    public void OpenMultiplayerBattle()
    {
      //UIManagerComponent.Instance.OpenMultiplayerBattle();
    }
  
    public void ExitFromGame()
    {
#if UNITY_EDITOR
      UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_ANDROID
      Application.Quit();
      System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
    }
  
    private void OnOpenBattle()
    {
      BattleControler.Instance.OnOpenBattle -= OnOpenBattle;
      UIManager.Instance.OpenSingleBattle();
    }
  }
}

