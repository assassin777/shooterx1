﻿using Scripts.Common;
using Scripts.Controler;
using Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.View.Managers
{
  public class UIManager : Singleton<UIManager>
  {
    private UIManager() { }
  
  	[SerializeField] private GameObject MainMenu    = null;
    [SerializeField] private GameObject SingleGame  = null;
    [SerializeField] private GameObject AfterBattle = null;
    [SerializeField] private GameObject Multiplayer = null;
  
    private Image afterBattleImage;
  
    [SerializeField] private Sprite winSprite;
    [SerializeField] private Sprite loseSprite;
  
    private void Awake()
    {
      afterBattleImage = AfterBattle.GetComponent<Image>();
    }
  
    private void Start()
    {
      BattleControler.Instance.OnCloseBattle += OnCloseBattle;
    }
  
    public void OpenMainMenu()
    {
      OpenPanel( UIPanels.MainMenuPanel );
    }
  
    public void OpenSingleBattle()
    {
      OpenPanel( UIPanels.SingleGamePanel );
    }
  
    public void OpenMultiplayerBattle()
    {
      OpenPanel( UIPanels.MultiplayerPanel );
    }
  
    private void OpenPanel( UIPanels opened )
    {
      MainMenu   .SetActive( opened == UIPanels.MainMenuPanel );
      SingleGame .SetActive( opened == UIPanels.SingleGamePanel );
      AfterBattle.SetActive( opened == UIPanels.AfterBattle );
      //Multiplayer.SetActive( opened == UIPanels.MultiplayerPanel );
    }
  
    private void OnCloseBattle( Battle.TeamWon team )
    {
      OpenPanel( UIPanels.AfterBattle );
  
      afterBattleImage.sprite = team == Battle.TeamWon.First ? winSprite : loseSprite;
    }
  
    private enum UIPanels
    {
      MainMenuPanel,
      SingleGamePanel,
      AfterBattle,
      MultiplayerPanel
    }
  }
}
