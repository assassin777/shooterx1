﻿using Scripts.Controler;
using Scripts.Model;
using UnityEngine;

namespace Scripts.View.Managers
{
  public class BattleManager : MonoBehaviour
  {
    [SerializeField] private GameObject location;
  
    private void Awake()
    {
      BattleControler.Instance.OnOpenBattle  += OnOpenBattle;
      BattleControler.Instance.OnCloseBattle += OnCloseBattle;
  
      location.SetActive( false );
    }
  
    private void OnCloseBattle( Battle.TeamWon team )
    {
      location.SetActive( false );
    }
  
    private void OnOpenBattle()
    {
      location.SetActive( true );
    }
  }
}

