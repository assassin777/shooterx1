﻿using Scripts.Common;
using Scripts.View.Components;
using UnityEngine;

namespace Scripts.View.Managers
{
  public class BulletsManager : Singleton<BulletsManager>
  {
    [SerializeField] private GameObject bulletPrefab;
  
    [SerializeField] private float bulletSpeed;
  
  	public void Shoot( Vector2 startPosition, Vector2 directon, int damage, string target )
    {
      GameObject bullet = Instantiate( bulletPrefab, transform );
      bullet.transform.localPosition = new Vector3( startPosition.x, startPosition.y, 0 );
      bullet.GetComponent<BulletComponent>().Adjust( directon, target, bulletSpeed, damage );
    }
  }
}
