﻿using System;
using System.Collections;
using Scripts.View.Managers;
using UnityEngine;

namespace Scripts.View.Components
{
  public class WarriorComponent : MonoBehaviour
  {
    private const string Walk_AnimParamName = "Walk";
    private const string Hit_AnimParamName = "Hit";
    private const string Shoot_AnimParamName = "Shoot";
    private const string Death_AnimParamName = "Death";
  
    private const float DyingTimeInSeconds = 2f;
    private const float HitTime = 0.5f;
    private const float ShootTime = 0.5f;
  
    private float timerHit = 0;
    private float timerShoot = 0;
  
    private Animator animator;
    private Vector2 direction = Vector2.zero;
  
    private float moveRange;
    private float speed;
    private bool isWalk;
    private bool isHit;
    private bool isShoot;
  
    public event Action<int> OnHit;
  
    private void Awake()
    {
      animator = GetComponent<Animator>();
    }
  
    public void Adjust( Vector2 startPosition, float moveRange, Vector2 shootDirection, float speed )
    {
      isWalk = false;
      isHit = false;
      isShoot = false;
  
      this.speed = speed;
      transform.localPosition = startPosition;
      transform.localScale = new Vector2( transform.localScale.x * shootDirection.x, transform.localScale.y );
      this.moveRange = moveRange;
    }
  
    public void Shoot( Vector2 shootDirection, int damage, string target )
    {
      StopWalk();
      animator.SetTrigger( Shoot_AnimParamName );
      BulletsManager.Instance.Shoot( transform.position, shootDirection, damage, target );
  
      isShoot = true;
    }
  
    public void Hit()
    {
      StopWalk();
      animator.SetTrigger( Hit_AnimParamName );
  
      isHit = true;
    }
  
    public void Death()
    {
      animator.SetTrigger( Death_AnimParamName );
      StartCoroutine( Dying() );
  
      StopWalk();
    }
  
    public void BulletColliderTriger( int damage )
    {
      OnHit( damage );
    }
  
    public void Walk( MoveDirection moveDirection, bool isWalk )
    {
      this.isWalk = isWalk;
      Walk();
  
      direction = moveDirection == MoveDirection.Left ? Vector2.up : Vector2.down;
    }
  
    private void Update()
    {
      if ( isHit )
      {
        if ( timerHit > HitTime )
        {
          isHit = false;
          timerHit = 0;
        } else
        {
          timerHit += Time.deltaTime;
        }
      } else
      if ( isShoot )
      {
        if ( timerShoot > ShootTime )
        {
          isShoot = false;
          timerShoot = 0;
        } else
        {
          timerShoot += Time.deltaTime;
        }
      } else
      if ( isWalk )
      {
        Vector2 newPosition = transform.localPosition + (Vector3)( direction * speed * Time.fixedDeltaTime );
        transform.localPosition = new Vector3( newPosition.x,  Mathf.Clamp( newPosition.y, -moveRange, moveRange ) );
      }
    }
  
    private IEnumerator Dying()
    {
      yield return new WaitForSeconds( DyingTimeInSeconds );
      Destroy( gameObject );
    }
  
    private void StopWalk()
    {
      if ( isWalk )
      {
        isWalk = false;
        Walk();
      }
    }
  
    private void Walk()
    {
      animator.SetBool( Walk_AnimParamName, isWalk );
    }
  
    public enum MoveDirection
    {
      Left,
      Right,
      Unknown
    }
  }
}
