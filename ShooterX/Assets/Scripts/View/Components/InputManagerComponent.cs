﻿using Scripts.View.Managers;
using UnityEngine;

namespace Scripts.View.Components
{
  public class InputManagerComponent : MonoBehaviour
  {
  	private void Update()
    {
  		if ( Input.GetKeyDown( KeyCode.Escape ) )
      {
        GameManager.Instance.ExitFromGame();
      }
  	}
  }
}

