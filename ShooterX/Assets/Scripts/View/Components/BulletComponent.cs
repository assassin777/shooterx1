﻿using Scripts.Common;
using UnityEngine;

namespace Scripts.View.Components
{
  public class BulletComponent : MonoBehaviour
  {
    private string target;
    private Vector2 direction;
    private float bulletSpeed;
    private int damage;
  
  	public void Adjust( Vector2 directon, string target, float bulletSpeed, int damage )
    {
      this.direction = directon;
      this.target = target;
      this.bulletSpeed = bulletSpeed;
      this.damage = damage;
    }
  
  	private void Update()
    {
      transform.localPosition += (Vector3)( direction * bulletSpeed * Time.fixedDeltaTime );
  	}
  
    private void OnTriggerEnter2D( Collider2D other )
    {
      if ( other.gameObject.CompareTag( target ) )
      {
        other.gameObject.GetComponent<WarriorComponent>().BulletColliderTriger( damage );
        Destroy( gameObject );
      } else
      if ( other.gameObject.CompareTag( TagsName.Wall ) )
      {
        Destroy( gameObject );
      }
    }
  }
}

